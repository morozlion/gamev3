﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game_V3
{
    internal class Traps : Trap
    {
        public Traps(int x, int y, char ch, int damage) : base(x, y, ch, damage) { }
        public readonly List<Trap> Tr = new List<Trap>();
        private readonly Random _rnd = new Random();
        public void CreateTraps()
        {
            while (Tr.Count < 10)
            {
                X = _rnd.Next(1, 10);
                Y = _rnd.Next(1, 10);
                if ((X == 1 || Y == 1) || (X == 10 || Y == 10)) continue;
                var trap = new Trap(X, Y, Ch, _rnd.Next(1, 10));
                var l = true;
                foreach (var t in Tr.Where(t => X == t.X && Y == t.Y))
                {
                    l = false;
                }
                if (l) Tr.Add(trap);
                //trap.Draw();//рисует на поле ловушки
            }
        }
    }
}