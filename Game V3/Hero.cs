﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game_V3
{
    internal class Hero : Point
    {
        public int Health { get; private set; }
        public Hero(int x, int y, char ch, int health) : base(x, y, ch)
        {
            Health = health;
        }
        public void Move(ConsoleKey key, IEnumerable<Brick> wl)
        {
            switch (key)
            {
                case ConsoleKey.DownArrow:
                    {
                        var l = true;
                        foreach (var w in wl.Where(w => w.X == X && w.Y == (Y + 1)))
                        {
                            l = false;
                            Console.Beep(200, 300);
                        }
                        if (l) Y++;
                    }
                    break;
                case ConsoleKey.UpArrow:
                    {
                        var l = true;
                        foreach (var w in wl.Where(w => (w.X == X) && w.Y == (Y - 1)))
                        {
                            l = false;
                            Console.Beep(200, 300);
                        }
                        if (l) Y--;
                    }
                    break;
                case ConsoleKey.RightArrow:
                    {
                        var l = true;
                        foreach (var w in wl.Where(w => w.X == (X + 1) && w.Y == Y))
                        {
                            l = false;
                            Console.Beep(200, 300);
                        }
                        if (l) X++;
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    {
                        var l = true;
                        foreach (var w in wl.Where(w => w.X == (X - 1) && w.Y == Y))
                        {
                            l = false;
                            Console.Beep(200, 300);
                        }
                        if (l) X--;
                    }
                    break;
                default:
                    {
                        Console.Beep(200, 300);
                    }
                    break;
            }
        }
        public void OverlapTr(IEnumerable<Trap> tr)
        {
            foreach (var t in tr)
            {
                if (t.X != X || t.Y != Y) continue;
                Health -= t.Damage;
                if (Health > 0) continue;
                Console.SetCursorPosition(0, 13);
                Console.WriteLine("Game Over!!!");
                throw new Exception("game over");
            }
        }
        public void Victory(int x, int y)
        {
            if (X != x || Y != y) return;
            Console.SetCursorPosition(0, 13);
            Console.WriteLine("Win!!!");
            throw new Exception("victory");
        }
    }
}