﻿namespace Game_V3
{
    internal class Trap : Point
    {
        public int Damage { get; private set; }

        protected internal Trap(int x, int y, char ch, int damage) : base(x, y, ch)
        {
            Damage = damage;
        }
    }
}