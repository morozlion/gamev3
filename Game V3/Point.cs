﻿using System;

namespace Game_V3
{
    internal abstract class  Point
    {
        public int X { get; set; }
        public int Y { get; set; }
        protected char Ch { get; private set; }

        protected Point(int x, int y, char ch)
        {
            X = x;
            Y = y;
            Ch = ch;
        }
        public void Draw()
        {
            DrawPoint(Ch);
        }
        public void Clear()
        {
            DrawPoint(' ');
            Console.SetCursorPosition(X, Y);
        }
        private void DrawPoint(char ch)
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(Ch);
        }
    }
}