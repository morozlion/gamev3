﻿using System;

namespace Game_V3
{
    internal static class Program
    {
        private static readonly int x = 10;
        private static readonly int y = 10;
        private static Hero hero;
        private static Princess princess;
        private static Walls walls;
        private static Traps traps;

        private static void Main(string[] args)
        {
            var game = true;
            var score = new int[2] { 0, 0 };
            while (game)
            {
                Console.Clear();
                Console.CursorVisible = false;
                hero = new Hero(1,1,'&',10);
                hero.Draw();
                princess = new Princess(10, 10, '$');
                princess.Draw();
                walls = new Walls(x+1, y+1, '#');
                traps = new Traps(x,y,'@',0);
                traps.CreateTraps();
                Console.SetCursorPosition(1, 1);
                while (true)
                {
                    if (!Console.KeyAvailable) continue;
                    hero.Clear();
                    var key = Console.ReadKey();
                    {
                        try
                        {
                            hero.Move(key.Key, walls.walls);
                            hero.Draw();
                            hero.OverlapTr(traps.Tr);
                            hero.Victory(princess.X, princess.Y);
                            Console.SetCursorPosition(0, 12);
                            Console.Write(new string(' ', Console.BufferWidth));
                            Console.SetCursorPosition(0, 12);
                            Console.Write("your health={0}", hero.Health);
                        }
                        catch (Exception e)
                        {
                            Console.SetCursorPosition(0, 16);
                            if (e.Message == "game over")
                            {
                                score[1]++;
                                Console.WriteLine("Счет: {0}-{1}", score[0], score[1]);
                                game = Again();
                                break;
                            }

                            if (e.Message != "victory") continue;
                            score[0]++;
                            Console.WriteLine("Счет: {0}-{1}", score[0], score[1]);
                            game = Again();
                            break;
                        }
                    }
                }
            }
        }

        private static bool Again()
        {
            Console.WriteLine("Желатете сыграть снова? Нажмите 'Y' чтобы сыграть еще, или нажмите любую клавишу чтобы выйти из игры (Выбор подтвердите нажатием ENTER): ");
            var key = Console.ReadLine();
            switch (key)
            {
                case "y":
                    {
                        return true;
                    }
                default:
                    {
                        return false;
                    }
            }
        }
    }
}