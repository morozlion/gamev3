﻿using System.Collections.Generic;

namespace Game_V3
{
    internal class Walls : Brick
    {
        public readonly List<Brick> walls = new List<Brick>();
        public Walls(int x, int y, char ch) : base(x, y, ch)
        {
            DrawHorizontal(x, 0);
            DrawHorizontal(x, y);
            DrawVertical(0, y);
            DrawVertical(x, y);
        }
        private void DrawHorizontal(int x, int y)
        {
            for (var i = 0; i < x; i++)
            {
                var brick = new Brick(i, y, Ch);
                brick.Draw();
                walls.Add(brick);
            }
        }
        private void DrawVertical(int x, int y)
        {
            for (var i = 0; i < y; i++)
            {
                var brick = new Brick(x, i, Ch);
                brick.Draw();
                walls.Add(brick);
            }
        }

    }
}